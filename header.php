<head>
    <style>
        .selected
        {
            background-color: gold!important;
        }
        table, th, td {
            border: 1px solid black;
            /*border-collapse: collapse;*/
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color:#fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
            font-weight: 100;
            text-align: center;
            padding:11px;
        }
        td{
            padding:12px!important;
        }
        input, textarea {
            font-size: 14px;
            width: 100%;
            height: 100%;
            padding: 5px 10px;
            background: none;
            font-family: iranyekan!important;
            background-image: none;
            border: 1px solid #a0b3b0;
            border-radius: 0;
            -webkit-transition: border-color .25s ease, box-shadow .25s ease;
            transition: border-color .25s ease, box-shadow .25s ease;
        }
        .form {
            background: rgba(19, 35, 47, 0.9);
            padding: 40px;
            max-width: 600px;
            margin: 10px auto;
            border-radius: 4px;
            box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
        }
        .button {
            border: 0;
            outline: none;
            border-radius: 0;
            padding: 15px 0;
            font-size: 20px;
            background: #1ab188;
            color: #ffffff;
        }
        .button:hover, .button:focus {
            background: #179b77;
        }

        .button-block {
            margin: 16px 16px;
            display: inline;
            width: 50%;
            font-family: iranyekan;
        }
        #page-inner {
            text-align: center !important;}
    </style>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>پنل مدیریتی سامانه اجاره ویدیو</title>
    <link href="admin/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="admin/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="admin/assets/css/basic.css" rel="stylesheet" />
    <style>
        .nav01
        {
            padding-top: 10px;
        }
        .nav01 li
        {
            padding-right: 43px;
            color:#fff;
            list-style: none;

        }
        .nav01 li a
        {
            text-decoration: none;
        }
    </style>
</head>
<body>
<div id="wrapper">

    <nav>
        <ul>
            <li><a href="admin_main_panel.php">صفحه اصلی</a></li>

            <li><a href="product.php">محصولات</a>
                <div>
                    <ul>
                        <li><a href="borrow_product.php">امانتی ها</a></li>
                        <li><a href="reserve_product.php">رزروی ها</a></li>
                        <li><a href="#">حذف</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </nav>
    <div id="page-wrapper">
        <div id="page-inner">