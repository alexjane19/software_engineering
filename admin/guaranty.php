﻿<?php
if(!isset($_SESSION)) {
    session_start();
}?>
    <style>
        table, th, td {
            border: 1px solid black;
            /*border-collapse: collapse;*/
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color:#fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
            font-weight: 100;
            text-align: center;
            padding:11px;
        }
        td{
            padding:12px!important;
        }
        input, textarea {
            font-size: 14px;
            width: 100%;
            height: 100%;
            padding: 5px 10px;
            background: none;
            font-family: iranyekan!important;
            background-image: none;
            border: 1px solid #a0b3b0;
            border-radius: 0;
            -webkit-transition: border-color .25s ease, box-shadow .25s ease;
            transition: border-color .25s ease, box-shadow .25s ease;
        }
        .form {
            background: rgba(19, 35, 47, 0.9);
            padding: 40px;
            max-width: 600px;
            margin: 10px auto;
            border-radius: 4px;
            box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
        }
        .button {
            border: 0;
            outline: none;
            border-radius: 0;
            padding: 15px 0;
            font-size: 20px;
            background: #1ab188;
            color: #ffffff;
        }
        .button-block {
            display: block;
            width: 100%;
            font-family: iranyekan;
        }
        .button:hover, .button:focus {
            background: #179b77;
        }

    </style>
<?php
require_once "../config.php";
require "header.php";
$sql = "SELECT * FROM order_products";
$result = mysqli_query($conn,$sql);
if (mysqli_num_rows($result)>0) {
    echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
					    <th>نام مشتری</th>
						<th>نام محصول</th>
						<th>تاریخ</th> 
						<th>وضعیت</th>
					</tr>
			';
    while ($rows = mysqli_fetch_assoc($result)) {
        $count = 1;
        $state = $rows["state"];
        $msg_state = "";
        if ($state == "1"){
            $msg_state = "رزرو";
        }else if ($state == "2"){
            $msg_state = "اجاره داده شده";
        }
        echo '
					<tr>
						<td>' . $rows["username"] . '</td>
						<td>' . $rows["name_pro"] . '</td> 
						<td>' . $rows["date_order"] . '</td>
						<td><button class="button button-block" title="'.$state.'" name="'. $rows["id"]. '" id="submit"/>' . $msg_state . '</button></td>

					</tr>
				';
    }
    echo '</table>';
}
else{
    echo '<div style="text-align:center">کاربری برای نمایش وجود ندارد</div>';
}
echo '<script src="assets/js/jquery-1.10.2.js"></script> <script type="text/javascript" language="javascript">     $("#submit").click(function () {
        var state = $("#submit").attr("title");
        var name = $("#submit").attr("name");
        $.post("../order_product.php", {"idorder": name, "state": state} , function(result){
            if(state == 1){
                $("#submit").html("اجاره داده شده");
                $("#submit").attr("title", 2);
            }else if(state == 2){
               location.reload(); 
            }
            console.log(result);
//            $("span").html(result);
        });
    }); </script>';
require "footer.php";