<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>پنل مدیریتی سامانه اجاره ویدیو</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/basic.css" rel="stylesheet" />
    <style>
        .nav01
        {
            padding-top: 10px;
        }
        .nav01 li
        {
            padding-right: 43px;
            color:#fff;
            list-style: none;

        }
        .nav01 li a
        {
            text-decoration: none;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <nav>
        <ul>
            <li><a href="admin_main_panel.php">صفحه اصلی</a></li>
            <li><a href="users.php">مشتریان</a>
                <div>
                    <ul>
                        <li><a href="adduser.php">افزودن</a></li>
                        <li><a href="edit_users.php">ویرایش</a></li>
                        <li><a href="delete_user.php">حذف</a></li>
                    </ul>
                </div>
            </li>
            <li><a href="products.php">محصولات</a>
                <div>
                    <ul>
                        <li><a href="add_dvds.php">افزودن</a></li>
                        <li><a href="edit_products.php">ویرایش</a></li>
                        <li><a href="delete_dvds.php">حذف</a></li>
                        <li><a href="search_products.php">جست و جو</a></li>
                    </ul>
                </div>
            </li>
            <li><a href="guaranty.php">امانات</a>
            <div>
                <ul>
                    <li><a href="search_quaranty.php">جست و جو</a></li>
                </ul>
            </div>
            </li>
        </ul>
    </nav>
    <div id="page-wrapper">
        <div id="page-inner">