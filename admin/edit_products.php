﻿<?php
if(!isset($_SESSION)) {
    session_start();
}
require_once "../config.php";
require "header.php";
?>
    <style>
        table, th, td {
            border: 1px solid black;
            /*border-collapse: collapse;*/
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color:#fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
            font-weight: 100;
        }
        .form {
            background: rgba(19, 35, 47, 0.9);
            padding: 40px;
            max-width: 600px;
            margin: 10px auto;
            border-radius: 4px;
            box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
        }
        label {
            color: #fff;
            font-size: 15px;
        }
        label .req {
            margin: 2px;
            color: #1ab188;
        }

        label.active {
            display:none!important;
        }
        label.active .req {
            opacity: 0;
        }
        label.highlight {
            color: #ffffff;
        }
        input, textarea {
            font-size: 14px;
            width: 100%;
            height: 100%;
            padding: 5px 10px;
            background: none;
            font-family: iranyekan!important;
            background-image: none;
            border: 1px solid #a0b3b0;
            border-radius: 0;
            -webkit-transition: border-color .25s ease, box-shadow .25s ease;
            transition: border-color .25s ease, box-shadow .25s ease;
        }
        input:focus, textarea:focus {
            outline: 0;
            border-color: #1ab188;
        }

        textarea {
            border: 2px solid #a0b3b0;
            resize: vertical;
        }

        .field-wrap {
            position: relative;
            margin-bottom: 40px;
        }

        .top-row:after {
            content: "";
            display: table;
            clear: both;
        }
        .top-row > div {
            float: right;
            width: 48%;
            margin-left: 4%;
        }
        .top-row > div:last-child {
            margin: 0;
        }
        .button {
            border: 0;
            outline: none;
            border-radius: 0;
            padding: 15px 0;
            font-size: 20px;
            background: #1ab188;
            color: #ffffff;
        }
        .button:hover, .button:focus {
            background: #179b77;
        }

        .button-block {
            display: block;
            width: 100%;
            font-family: iranyekan;
        }
        #se_arch{
            color:#fff;
        }
    </style>
    <div class="form">
        <div class="tab-content">
            <div id="login">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="field-wrap">
                        <label style="float: right"> : نوع محصول را انتخاب نمایید</label>
                        <select name="input_type" required style="width:20%;background-color: gainsboro;margin-right: 20px">
                            <option value="فیلم">فیلم</option>
                            <option value="بازی">بازی</option>
                        </select>
                    </div>
                    <div class="field-wrap">
                        <label> : نام محصول را وارد نمایید</label>
                        <input style="color:#fff;text-align: right" name="input_name" type="text" required /><br/>
                    </div>
                    <button class="button button-block" name="search"/>جست و جو</button>
                </form>
            </div>
        </div>
    </div>
<?php
if (count($_POST) > 0 && isset($_POST['search'])) {
    $search_term_name = $_POST['input_name'];
    $search_term_type = $_POST['input_type'];

    $sql = "SELECT * FROM products_data WHERE name='".$search_term_name."' AND type='".$search_term_type."'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $_SESSION["found_dvd_name"] = $search_term_name;
        $_SESSION["found_dvd_type"] = $search_term_type;
        echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
					    <th>نوع محصول</th>
						<th>نام</th>
						<th>تعداد نسخه</th> 
						<th>تاریخ تولید</th>
					</tr>
			';
        while ($rows = mysqli_fetch_assoc($result)) {
            echo '
					<tr>'; ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
            <?php echo '
						<td>' . $rows["type"] . '</td>
						<td>' . $rows["name"] . '</td>
						<td><input name="dvd_count" type="text" value="' . $rows["dvd_count"] . '"><br/></td>
						<td><input name="published_year" type="text" value="' . $rows["published_year"] . '"><br/></td>
					</tr>
				';
            //."<br/>";
        }
        echo '</table><input id="b01" type="submit" name="edit" value="ویرایش"></form>';
    } else {
        echo '<div style="text-align:center">محصولی برای نمایش وجود ندارد</div>';
    }
}

if (count($_POST) > 0 && isset($_POST['edit'])){
    $sql = "UPDATE products_data SET dvd_count='".$_POST['dvd_count']."',published_year='".$_POST['published_year']."' WHERE name='".$_SESSION["found_dvd_name"]."' AND type='".$_SESSION["found_dvd_type"]."'";
    $result = mysqli_query($conn,$sql);
    echo '<div style="text-align:center">ویرایش محصول مورد نظر با موفقیت انجام پذیرفت</div>';
}
require "footer.php";