<?php
if(!isset($_SESSION)) {
    session_start();
}
require_once "../config.php";
require "header.php";
?>
    <style>
        table, th, td {
            border: 1px solid black;
            /*border-collapse: collapse;*/
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color:#fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
            font-weight: 100;
        }
        .form {
            background: rgba(19, 35, 47, 0.9);
            padding: 40px;
            max-width: 600px;
            margin: 10px auto;
            border-radius: 4px;
            box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
        }
        label {
            color: #fff;
            font-size: 15px;
        }
        label .req {
            margin: 2px;
            color: #1ab188;
        }

        label.active {
            display:none!important;
        }
        label.active .req {
            opacity: 0;
        }
        label.highlight {
            color: #ffffff;
        }
        input, textarea {
            font-size: 14px;
            width: 100%;
            height: 100%;
            padding: 5px 10px;
            background: none;
            font-family: iranyekan!important;
            background-image: none;
            border: 1px solid #a0b3b0;
            border-radius: 0;
            -webkit-transition: border-color .25s ease, box-shadow .25s ease;
            transition: border-color .25s ease, box-shadow .25s ease;
        }
        input:focus, textarea:focus {
            outline: 0;
            border-color: #1ab188;
        }

        textarea {
            border: 2px solid #a0b3b0;
            resize: vertical;
        }

        .field-wrap {
            position: relative;
            margin-bottom: 40px;
        }

        .top-row:after {
            content: "";
            display: table;
            clear: both;
        }
        .top-row > div {
            float: right;
            width: 48%;
            margin-left: 4%;
        }
        .top-row > div:last-child {
            margin: 0;
        }
        .button {
            border: 0;
            outline: none;
            border-radius: 0;
            padding: 15px 0;
            font-size: 20px;
            background: #1ab188;
            color: #ffffff;
        }
        .button:hover, .button:focus {
            background: #179b77;
        }

        .button-block {
            display: block;
            width: 100%;
            font-family: iranyekan;
        }
        #se_arch{
            color:#fff;
        }
    </style>
    <div class="form">
        <div class="tab-content">
            <div id="login">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="field-wrap">
                        <label> : شناسه کاربر و یا نام خانوادگی او را جهت ویرایش وارد نمایید</label>
                        <input id="se_arch" name="input" type="text" required /><br/>
                    </div>
                    <button class="button button-block" name="search"/>جست و جو</button>
                </form>
            </div>
        </div>
    </div>
<?php
if (count($_POST) > 0 && isset($_POST['search'])) {
    $search_term = $_POST['input'];
    if(is_numeric($search_term)) { // Yani Shenaseye Karbar Vared Shode Ast
        $sql = "SELECT * FROM users_data WHERE username='" . $search_term . "'";
    }
    else{ // Yani Name Khanevadegiye Karbar Vared Shode Ast
        $sql = "SELECT * FROM users_data WHERE lastname='" . $search_term . "'";
    }
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $_SESSION["found_username"] = $search_term;
        echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
					    <th>شناسه کاربری</th>
						<th>نام</th>
						<th>نام خانوادگی</th> 
						<th>آدرس</th>
						<th>شماره تلفن</th>
					</tr>
			';
        while ($rows = mysqli_fetch_assoc($result)) {
            echo '
					<tr>
					    <td>' . $rows["username"] . '</td>'; ?>
					    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                        <?php echo '
						<td><input name="firstname" type="text" value="' . $rows["firstname"] . '"><br/></td>
						<td><input name="lastname" type="text" value="' . $rows["lastname"] . '"><br/></td>
						<td><input name="address" type="text" value="' . $rows["address"] . '"><br/></td>
						<td><input name="phone_number" type="text" value="' . $rows["phone_number"] . '"><br/></td>
					</tr>
				';
            //."<br/>";
        }
        echo '</table><input id="b01" type="submit" name="edit" value="ویرایش"></form>';
    } else {
        echo '<div style="text-align:center">کاربری برای نمایش وجود ندارد</div>';
    }
}

if (count($_POST) > 0 && isset($_POST['edit'])){
    $sql = "UPDATE users_data SET firstname='".$_POST['firstname']."',lastname='".$_POST['lastname']."',address='".$_POST['address']."',phone_number='".$_POST['phone_number']."' WHERE username='".$_SESSION["found_username"]."';";
    $result = mysqli_query($conn,$sql);
    echo '<div style="text-align:center">ویرایش کاربر مورد نظر با موفقیت انجام پذیرفت</div>';
}
require "footer.php";