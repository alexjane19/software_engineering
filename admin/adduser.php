﻿<?php
if(!isset($_SESSION)) {
    session_start();
}
require_once "../config.php";
require "header.php";
?>
<style>
    .form {
        background: rgba(19, 35, 47, 0.9);
        padding: 40px;
        max-width: 600px;
        margin: 10px auto;
        border-radius: 4px;
        box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
    }
    label {
        color: #fff;
        font-size: 15px;
    }
    label .req {
        margin: 2px;
        color: #1ab188;
    }

    label.active {
        display:none!important;
    }
    label.active .req {
        opacity: 0;
    }
    label.highlight {
        color: #ffffff;
    }
    input, textarea {
        font-size: 22px;
        display: block;
        width: 100%;
        height: 100%;
        padding: 5px 10px;
        background: none;
        font-family: iranyekan!important;
        background-image: none;
        border: 1px solid #a0b3b0;
        color: #ffffff;
        border-radius: 0;
        -webkit-transition: border-color .25s ease, box-shadow .25s ease;
        transition: border-color .25s ease, box-shadow .25s ease;
    }
    input:focus, textarea:focus {
        outline: 0;
        border-color: #1ab188;
    }

    textarea {
        border: 2px solid #a0b3b0;
        resize: vertical;
    }

    .field-wrap {
        position: relative;
        margin-bottom: 40px;
    }

    .top-row:after {
        content: "";
        display: table;
        clear: both;
    }
    .top-row > div {
        float: right;
        width: 48%;
        margin-left: 4%;
    }
    .top-row > div:last-child {
        margin: 0;
    }
    .button {
        border: 0;
        outline: none;
        border-radius: 0;
        padding: 15px 0;
        font-size: 20px;
        background: #1ab188;
        color: #ffffff;
    }
    .button:hover, .button:focus {
        background: #179b77;
    }

    .button-block {
        display: block;
        width: 100%;
        font-family: iranyekan;
    }
</style>

    <div class="form">
        <div class="tab-content">
            <div id="login">
                <form style="direction:rtl" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="field-wrap">
                        <label><span class="req">*</span>نام</label>
                        <input name="firstname" type="text" required><br/>
                    </div>
                    <div class="field-wrap">
                        <label><span class="req">*</span>نام خانوادگی</label>
                        <input name="lastname" type="text" required><br/>
                    </div>
                    <div class="field-wrap">
                        <label><span class="req">*</span>آدرس</label>
                        <input name="address" type="text" required><br/>
                    </div>
                    <div class="field-wrap">
                        <label><span class="req">*</span>شماره تلفن</label>
                        <input name="phone_number" type="text" required><br/>
                    </div>
                    <button class="button button-block" name="submit"/>ثبت</button>
                </form>
            </div>
        </div><!-- tab-content -->
    </div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="../js/index.js"></script>

<?php
if (count($_POST) > 0 && isset($_POST['submit'])){
    $ph_num = $_POST['phone_number'];


    $result = mysqli_query($conn, "SELECT phone_number FROM users_data WHERE phone_number='$ph_num'");
    $registered_before = FALSE;
    while(mysqli_fetch_array($result)) {
        $registered_before = TRUE;
    }
    if ($registered_before == FALSE) {
        $sql = "INSERT INTO users_data(firstname,lastname,address,phone_number) VALUES ('" . $_POST['firstname'] . "','" . $_POST['lastname'] . "','" . $_POST['address'] . "','" . $_POST['phone_number'] . "');";
        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        echo '<div style="text-align:center">کاربر با موفقیت اضافه گردید</div>';
    }
    else{
        echo "<div style='text-align:center'>!کاربری قبلا با این شماره تلفن به ثبت رسیده است<div>";
    }
}
require "footer.php";