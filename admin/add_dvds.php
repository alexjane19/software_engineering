﻿<?php
if(!isset($_SESSION)) {
    session_start();
}
require_once "../config.php";
require "header.php";
?>
    <style>
        .form {
            background: rgba(19, 35, 47, 0.9);
            padding: 40px;
            max-width: 600px;
            margin: 10px auto;
            border-radius: 4px;
            box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
        }
        label {
            color: #fff;
            font-size: 15px;
        }
        label .req {
            margin: 2px;
            color: #1ab188;
        }

        label.active {
            display:none!important;
        }
        label.active .req {
            opacity: 0;
        }
        label.highlight {
            color: #ffffff;
        }
        input, textarea {
            font-size: 22px;
            display: block;
            width: 100%;
            height: 100%;
            padding: 5px 10px;
            background: none;
            font-family: iranyekan!important;
            background-image: none;
            border: 1px solid #a0b3b0;
            color: #ffffff;
            border-radius: 0;
            -webkit-transition: border-color .25s ease, box-shadow .25s ease;
            transition: border-color .25s ease, box-shadow .25s ease;
        }
        input:focus, textarea:focus {
            outline: 0;
            border-color: #1ab188;
        }

        textarea {
            border: 2px solid #a0b3b0;
            resize: vertical;
        }

        .field-wrap {
            position: relative;
            margin-bottom: 40px;
        }

        .top-row:after {
            content: "";
            display: table;
            clear: both;
        }
        .top-row > div {
            float: right;
            width: 48%;
            margin-left: 4%;
        }
        .top-row > div:last-child {
            margin: 0;
        }
        .button {
            border: 0;
            outline: none;
            border-radius: 0;
            padding: 15px 0;
            font-size: 20px;
            background: #1ab188;
            color: #ffffff;
        }
        .button:hover, .button:focus {
            background: #179b77;
        }

        .button-block {
            display: block;
            width: 100%;
            font-family: iranyekan;
        }
    </style>

    <div class="form">
        <div class="tab-content">
            <div id="login">
                <form style="direction:rtl" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="field-wrap">
                        <label><span class="req">*</span>نوع دی وی دی :</label>
                        <select name="dvd_type" required style="width:20%;background-color: gainsboro;margin-right: 20px">
                            <option value="فیلم">فیلم</option>
                            <option value="بازی">بازی</option>
                        </select>
                    </div>
                    <div class="field-wrap">
                        <label><span class="req">*</span>نام دی وی دی</label>
                        <input name="dvd_name" type="text" required><br/>
                    </div>
                    <div class="field-wrap">
                        <label><span class="req">*</span>تعداد نسخه ها</label>
                        <input name="dvd_count" type="text" required><br/>
                    </div>
                    <div class="field-wrap">
                        <label><span class="req">*</span>تاریخ تولید</label>
                        <input name="dvd_published_year" type="text" required><br/>
                    </div>
                    <button class="button button-block" name="submit"/>ثبت</button>
                </form>
            </div>
        </div><!-- tab-content -->
    </div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="../js/index.js"></script>

<?php
if (count($_POST) > 0 && isset($_POST['submit'])){
    $d_name = $_POST['dvd_name'];
    $d_type = $_POST['dvd_type'];
    // میتوانیم هم فیلمی به اسم تست داشته باشیم هم یک بازی به اسم تست
    $result = mysqli_query($conn, "SELECT name FROM products_data WHERE name='$d_name' AND type='$d_type'");
    $dvd_aded_before = FALSE;
    while(mysqli_fetch_array($result)) {
        $dvd_aded_before = TRUE;
    }
    if ($dvd_aded_before == FALSE) {
        $sql = "INSERT INTO products_data VALUES ('" . $_POST['dvd_type'] . "','" . $_POST['dvd_name'] . "','" . $_POST['dvd_count'] . "','" . $_POST['dvd_published_year'] . "');";
        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        echo '<div style="text-align:center">محصول با موفقیت اضافه گردید</div>';
    }
    else{
        echo "<div style='text-align:center'>!محصولی قبلا با این نام به ثبت رسیده است<div>";
    }
}
require "footer.php";