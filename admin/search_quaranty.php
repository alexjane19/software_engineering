﻿<?php
if(!isset($_SESSION)) {
    session_start();
}
require_once "../config.php";
require "header.php";
?>
    <style>
        table, th, td {
            border: 1px solid black;
            /*border-collapse: collapse;*/
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color:#fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
            font-weight: 100;
        }
        .form {
            background: rgba(19, 35, 47, 0.9);
            padding: 40px;
            max-width: 600px;
            margin: 10px auto;
            border-radius: 4px;
            box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
        }
        label {
            color: #fff;
            font-size: 15px;
        }
        label .req {
            margin: 2px;
            color: #1ab188;
        }

        label.active {
            display:none!important;
        }
        label.active .req {
            opacity: 0;
        }
        label.highlight {
            color: #ffffff;
        }
        input, textarea {
            font-size: 14px;
            width: 100%;
            height: 100%;
            padding: 5px 10px;
            background: none;
            font-family: iranyekan!important;
            background-image: none;
            border: 1px solid #a0b3b0;
            border-radius: 0;
            -webkit-transition: border-color .25s ease, box-shadow .25s ease;
            transition: border-color .25s ease, box-shadow .25s ease;
        }
        input:focus, textarea:focus {
            outline: 0;
            border-color: #1ab188;
        }

        textarea {
            border: 2px solid #a0b3b0;
            resize: vertical;
        }

        .field-wrap {
            position: relative;
            margin-bottom: 40px;
        }

        .top-row:after {
            content: "";
            display: table;
            clear: both;
        }
        .top-row > div {
            float: right;
            width: 48%;
            margin-left: 4%;
        }
        .top-row > div:last-child {
            margin: 0;
        }
        .button {
            border: 0;
            outline: none;
            border-radius: 0;
            padding: 15px 0;
            font-size: 20px;
            background: #1ab188;
            color: #ffffff;
        }
        .button:hover, .button:focus {
            background: #179b77;
        }

        .button-block {
            display: block;
            width: 100%;
            font-family: iranyekan;
        }
        #se_arch{
            color:#fff;
        }
    </style>
    <div class="form">
        <div class="tab-content">
            <div id="login">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="field-wrap">
                        <label style="float: right"> : نوع محصول را انتخاب نمایید</label>
                        <select name="input_type" required style="width:20%;background-color: gainsboro;margin-right: 20px">
                            <option value="فیلم">فیلم</option>
                            <option value="بازی">بازی</option>
                        </select>
                    </div>
                    <div class="field-wrap">
                        <label> : نام محصول یا نام مشتری را وارد نمایید</label>
                        <input style="color:#fff;text-align: right" name="input_name" type="text" required /><br/>
                    </div>
                    <button class="button button-block" name="search"/>جست و جو</button>
                </form>
            </div>
        </div>
    </div>
<?php
if (count($_POST) > 0 && isset($_POST['search'])) {
    $search_term_name = $_POST['input_name'];
    $search_term_type = $_POST['input_type'];

    $sql = "SELECT * FROM order_products WHERE name_pro='".$search_term_name."' OR username='".$search_term_name."'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $_SESSION["found_dvd_name"] = $search_term_name;
        $_SESSION["found_dvd_type"] = $search_term_type;
        echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
				    <th>نام مشتری</th>
						<th>نام محصول</th>
						<th>تاریخ</th> 
						<th>وضعیت</th>
					</tr>
			';
        while ($rows = mysqli_fetch_assoc($result)) {
            $state = $rows["state"];
            $msg_state = "";
            if ($state == "1"){
                $msg_state = "رزرو";
            }else if ($state == "2"){
                $msg_state = "برده شده";
            }
            echo '
					<tr>
						<td>' . $rows["username"] . '</td>
						<td>' . $rows["name_pro"] . '</td> 
						<td>' . $rows["date_order"] . '</td>
						<td><button class="button button-block" title="'.$state.'" name="'. $rows["id"]. '" id="submit"/>' . $msg_state . '</button></td>

					</tr>
				';
        }
        echo '</table>';
    } else {
        echo '<div style="text-align:center">محصولی برای نمایش وجود ندارد</div>';
    }
}

echo '<script src="assets/js/jquery-1.10.2.js"></script> <script type="text/javascript" language="javascript">     $("#submit").click(function () {
        var state = $("#submit").attr("title");
        var name = $("#submit").attr("name");
        $.post("../order_product.php", {"idorder": name, "state": state} , function(result){
            if(state == 1){
                $("#submit").html("برده شده");
                $("#submit").attr("title", 2);
            }else if(state == 2){
               location.reload(); 
            }
            console.log(result);
//            $("span").html(result);
        });
    }); </script>';
require "footer.php";