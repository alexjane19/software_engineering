﻿<?php
if(!isset($_SESSION)) {
    session_start();
}?>
<style>
    table, th, td {
        border: 1px solid black;
        /*border-collapse: collapse;*/
    }
    th, td {
        padding: 5px;
        text-align: center;
    }
    table#t01 tr:nth-child(even) {
        background-color: #eee;
    }
    table#t01 tr:nth-child(odd) {
        background-color:#fff;
    }
    table#t01 th {
        background-color: black;
        color: white;
        font-weight: 100;
        text-align: center;
        padding:11px;
    }
    td{
        padding:12px!important;
    }
    input, textarea {
        font-size: 14px;
        width: 100%;
        height: 100%;
        padding: 5px 10px;
        background: none;
        font-family: iranyekan!important;
        background-image: none;
        border: 1px solid #a0b3b0;
        border-radius: 0;
        -webkit-transition: border-color .25s ease, box-shadow .25s ease;
        transition: border-color .25s ease, box-shadow .25s ease;
    }
    .form {
        background: rgba(19, 35, 47, 0.9);
        padding: 40px;
        max-width: 600px;
        margin: 10px auto;
        border-radius: 4px;
        box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
    }
</style>
<?php
require_once "../config.php";
require "header.php";
$sql = "SELECT * FROM users_data";
$result = mysqli_query($conn,$sql);
if (mysqli_num_rows($result)>0) {
    echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
					    <th>نام</th>
						<th>نام خانوادگی</th>
						<th>آدرس</th> 
						<th>شماره تلفن</th>
						<th>شناسه کاربری</th>
					</tr>
			';
    while ($rows = mysqli_fetch_assoc($result)) {
        $count = 1;
        echo '
					<tr>
						<td>' . $rows["firstname"] . '</td>
						<td>' . $rows["lastname"] . '</td> 
						<td>' . $rows["address"] . '</td>
						<td>' . $rows["phone_number"] . '</td>
                        <td>' . $rows["username"] . '</td>
					</tr>
				';
        //."<br/>";
    }
    echo '</table>';
}
else{
    echo '<div style="text-align:center">کاربری برای نمایش وجود ندارد</div>';
}
require "footer.php";