<?php
if(!isset($_SESSION)) {
    session_start();
}
require_once '../config.php';
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>سامانه اجاره ویدیو</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <style>
        label .req {
            margin: 2px;
            color: #1ab188;
        }

        label.active {
            display:none!important;
        }
        label.active .req {
            opacity: 0;
        }
        label.highlight {
            color: #ffffff;
        }
    </style>
</head>
<body>
		<div class="grad"></div>
		<div class="header">
			<div>پنل مدیریت سامانه اجاره ویدیو</div><br/><br/>
		</div>
		<br>
        <div id="logo"></div>
        <div class="form">
            <div class="tab-content">
                <div id="login">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                        <div class="field-wrap">
                            <label><span class="req">*</span>شناسه کاربری</label>
                            <input type="text"required autocomplete="off" name="username"/>
                        </div>
                        <div class="field-wrap">
                            <label><span class="req">*</span>رمز عبور</label>
                            <input type="password"required autocomplete="off" name="password"/>
                        </div>
                        <button class="button button-block"/>ورود</button>
                    </form>
                </div>
            </div><!-- tab-content -->
        </div> <!-- /form -->
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="js/index.js"></script>
</body>
</html>
<?php
if (count($_POST) > 0){
    $username = mysqli_real_escape_string($conn,$_POST["username"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $result = mysqli_query($conn, "SELECT * FROM admin_data WHERE username='$username' AND password='$password'");
    $success = false;
    while($row = mysqli_fetch_array($result)) {
        $success = true;
    }
    if($success == true) {
        $_SESSION['username'] = $username;
        header("Location:admin_main_panel.php");
    }
    else{
        echo '<span class="info" style="z-index: 10000;position: relative;top:-150px;right:320px">اطلاعات وارد شده صحیح نمی باشد.لطفا مجددا تلاش نمایید!</span>';
    }
}