<?php
if(!isset($_SESSION)) {
    session_start();
}
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>سامانه اجاره ویدیو</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
</head>
<body>
		<div class="grad"></div>
		<div class="header">
			<div>سامانه اجاره ویدیو</div><br/><br/>
		</div>
		<br>
        <div id="logo"></div>
        <div class="form">
            <div class="tab-content">
                <div id="login">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                        <div class="field-wrap">
                            <label><span class="req">*</span>شناسه کاربری</label>
                            <input type="text"required autocomplete="off"/>
                        </div>
                        <div class="field-wrap">
                            <label><span class="req">*</span>رمز عبور</label>
                            <input type="password"required autocomplete="off"/>
                            <span class="info">* رمز عبور شما همان شماره تلفن شما میباشد</span>

                        </div>
                        <button class="button button-block"/>ورود</button>
                    </form>
                </div>
            </div><!-- tab-content -->
        </div> <!-- /form -->
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="js/index.js"></script>
</body>
</html>
<?php
if (count($_POST) > 0 && isset($_POST["username"]) && isset($_POST["password"])){
    require "curl.php";
}