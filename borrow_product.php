﻿<!DOCTYPE html>

<?php
if(!isset($_SESSION)) {
    session_start();
}

require_once "config.php";

    require "header.php";
    $sql = "SELECT * FROM order_products WHERE state='2' and username='alex'";
    $result = mysqli_query($conn,$sql);
    if (mysqli_num_rows($result)>0) {
        echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
						<th>نام محصول</th>
						<th>مدت گرفته شده</th>
						<th>هزینه</th> 
				<th>تاریخ دریافت</th>

						<th>تاریخ برگشت</th>
					</tr>
			';
        while ($rows = mysqli_fetch_assoc($result)) {
            date_default_timezone_set('Asia/Tehran');
            $now = date('Y-m-d H:i:s');
            $diffrent = strtotime($now) - strtotime($rows["date_order"]);
            $price = 0.2*$diffrent;
            $price = $price . " ریال";
            $diffrent /= 24*60*60;
            $ret = strtotime($rows["date_order"]) + 60*24*60*60;
            $ret = date('Y-m-d H:i:s', $ret);


            if ($diffrent<1)
                $diffrent = "کمتر از یک روز";
            else
                $diffrent = intval($diffrent) . " روز";
            $available = "";
//            if($rows["dvd_count"] > $rows["borrow"]){
//                $available = "موجود";
//            }else{
//                $available = "ناموجود";
//            }
            $count = 1;
            echo '
					<tr>
						<td>' . $rows["name_pro"] . '</td>
						<td>' . $diffrent . '</td> 
						<td>' . $price . '</td>
												<td>' . $rows["date_order"] . '</td>

						<td>' . $ret . '</td>
					</tr>
				';
        }
        echo '</table>';

    }
    else{
        echo '<div style="text-align:center">کاربری برای نمایش وجود ندارد</div>';
    }
require "footer.php";
?>


    <script type="text/javascript" language="javascript">

</script>

</body>
</html>