﻿<!DOCTYPE html>
<style>
    .button {
        border: 0;
        outline: none;
        border-radius: 0;
        padding: 15px 0;
        font-size: 20px;
        background: #1ab188;
        color: #ffffff;
    }
    .button-block {
        display: block!important;
        width: 100%!important;
        font-family: iranyekan;
        margin: 0 0!important;
    }
    .button:hover, .button:focus {
        background: #179b77;
    }
</style>
<?php
if(!isset($_SESSION)) {
    session_start();
}

require_once "config.php";

    require "header.php";
    $sql = "SELECT id, date_order,`type`, `name` FROM products_data JOIN (SELECT id, name_pro,date_order, count(*) as borrow FROM `order_products` WHERE state='1' and username='alex' GROUP BY name_pro) as t on products_data.name = t.name_pro";
    $result = mysqli_query($conn,$sql);
    if (mysqli_num_rows($result)>0) {
        echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
						<th>نام محصول</th>
						<th>نوع محصول</th>
						<th>تاریخ درخواست</th> 
				<th></th>
					</tr>
			';
        while ($rows = mysqli_fetch_assoc($result)) {
            $count = 1;
            $msg_state="لغو رزرو";
            echo '
					<tr>
						<td>' . $rows["name"] . '</td>
						<td>' . $rows["type"]  . '</td> 
						<td>' . $rows["date_order"]  . '</td>
						<td><button class="button button-block" name="'. $rows["id"]. '" id="submit"/>' . $msg_state . '</button></td>

					</tr>
				';
        }
        echo '</table>';

    }
    else{
        echo '<div style="text-align:center">کاربری برای نمایش وجود ندارد</div>';
    }
require "footer.php";
?>


    <script type="text/javascript" language="javascript">

</script>

</body>
</html>