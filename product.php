﻿<!DOCTYPE html>

<?php
if(!isset($_SESSION)) {
    session_start();
}

require_once "config.php";

    require "header.php";
        $sql = "SELECT `type`, `name`, `dvd_count`, `published_year`,COALESCE(`borrow`,0) AS borrow FROM products_data LEFT JOIN
(SELECT name_pro, count(*) as borrow FROM `order_products` GROUP BY name_pro) as t 
on products_data.name = t.name_pro";
    $result = mysqli_query($conn,$sql);
    if (mysqli_num_rows($result)>0) {
        echo '
			<table id="t01" style="width:100%;text-align: center;float:right;direction: rtl">
					<tr>
						<th>نام</th>
						<th>نوع محصول</th>

						<th>وضعیت</th> 
						<th>تاریخ تولید</th>
					</tr>
			';
        while ($rows = mysqli_fetch_assoc($result)) {
            $available = "";
            if($rows["dvd_count"] > $rows["borrow"]){
                $available = "موجود";
            }else{
                $available = "ناموجود";
            }
            $count = 1;
            echo '
					<tr>
						<td>' . $rows["name"] . '</td>
						<td>' . $rows["type"] . '</td> 
						<td>' . $available . '</td>
						<td>' . $rows["published_year"] . '</td>
					</tr>
				';
        }
        echo '</table>';
        echo '<button class="button button-block" name="submit" id="submit"/>ثبت</button>';

    }
    else{
        echo '<div style="text-align:center">کاربری برای نمایش وجود ندارد</div>';
    }
require "footer.php";
?>


    <script type="text/javascript" language="javascript">
    var myList = new Array();
    var isExist = false;

    $("#t01 tr").click(function(){
//        if($(this).find('td:nth-child(3)').html() == "ناموجود")
//            return;
        $(this).addClass('selected');
        var value=$(this).find('td:first').html();
        for (var i = myList.length - 1; i >= 0; i--) {
            if (myList[i] == value) {
                console.log(i);
                isExist = true;
                myList.splice(i, 1); ;
                $(this).removeClass('selected')
                break;
            }
        }
        console.log(isExist)

        if(!isExist)
            myList.push(value);
        else
            isExist =false;
        console.log(myList)
//        alert(value);
    });
    $('#submit').click(function () {

        console.log(myList);
        $.post("order_product.php", {"order": myList} , function(result){
            console.log(result);
//            $("span").html(result);
        });
    });

    $('.ok').on('click', function(e){
        alert($("#table tr.selected td:first").html());
    });
</script>

</body>
</html>